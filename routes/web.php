<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MaatwebsiteDemoController@importExport');

Route::get('/test', 'TableOneController@getmaxbatch');

Route::post('/roundtest', function () {
	
	$testnumber = Request::input('number_test');

	$roundednumber = ceil($testnumber);

	//$testnumber = 0.003;

	// if ($testnumber < 1 && $testnumber > 0) {
	// 	$roundednumber = 1;
	// }
	// else
	// {
	// 	$roundednumber = ceil($testnumber);
	// }

	echo $roundednumber;
});

// Route::post('/handle-form', 'TableOneController@importCsv');

Route::post('newcsvimport', 'NewCsvImportController@uploadUsers');

Route::post('unsubcsvimport', 'NewCsvImportController@uploadUnsubs');

Route::get('/updatetwo', 'TableOneController@moveToTableTwo');

Route::get('/updatethree', 'TableOneController@moveToTableThree');

Route::get('/endqueue', 'TableOneController@endqueue');

Route::get('/combinedmove', 'TableOneController@combinedmove');


// Route::get('/formtest', function () {
// 	return View::make('form');
// });

Route::get('/formroundtest', function () {
	return View::make('form');
});



Route::post('/posttest', function () {
	//var_dump(Request::file('import_file'));

	$name = Request::file('import_file')->getClientOriginalName();

	Request::file('import_file')->move('../storage/directory', $name);

	return 'Your File Was Moved';

	// return Request::file('import_file')->getFileName(); // temp file name

	// return Request::file('import_file')->getClientOriginalName(); // actual file name

	// return Request::file('import_file')->getClientSize(); // size of file in bytes

	// return Request::file('import_file')->getMimeType(); // full file type

	// return Request::file('import_file')->guessExtension(); // simple file type

	// return Request::file('import_file')->getRealPath(); // true location of uploaded file

	// return Request::file('import_file')->move('/storage/directory', $nameOfFile);
});



Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
Route::post('importExcel', 'MaatwebsiteDemoController@importExcel');
