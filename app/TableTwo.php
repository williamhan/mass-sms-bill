<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableTwo extends Model
{
    public $table = 'tabletwo';

    protected $fillable = ['phone_number', 'message_text', 'num_qty'];
}
