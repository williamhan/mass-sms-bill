<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableThree extends Model
{
    public $table = 'tablethree';

    public $fillable = [
        'row_number', 'batch_number', 'phone_number', 'message_text',
    ];
}
