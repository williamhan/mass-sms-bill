<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptOutNumber extends Model
{
    public $table = 'opt_out_numbers';
}
