<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableFour extends Model
{
    public $table = 'tablefour';

    public $fillable = [
        'sent_date', 'batch_number', 'phone_number', 'message_text',
    ];
}
