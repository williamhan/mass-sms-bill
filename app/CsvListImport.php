<?php

class CsvListImport extends \Maatwebsite\Excel\Files\ExcelFile {

	protected $delimiter = ',';
    protected $enclosure  = '"';
    protected $lineEnding = '\r\n';
	

    public function getFile()
{
    // Import a user provided file
    $file = Input::file('file');
    $filename = $this->doSomethingLikeUpload($file);

    // Return it's location
    return $filename;
}

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }

}