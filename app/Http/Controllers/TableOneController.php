<?php

namespace App\Http\Controllers;

use Request;
use App\TableOne;
use App\TableTwo;
use App\TableThree;
use App\TableFour;
use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Twilio\Rest\Client;

class TableOneController extends Controller
{
    public function dupecheck()
    {
        $users = DB::table('tableone')
                ->select('phone_number', DB::raw('count(*) as number_count'))
                ->groupBy('phone_number')
                ->havingRaw('count(*) > 1')
                ->get();

        foreach ($users as $user) {
            echo "Phone Number: " . $user->phone_number . " Number Of Instances: " . $user->number_count . "<br>";
            echo "<br>";
        }
    }

    public function dupedelete()
    {
        $userdels = DB::table('tableone')
                ->select('phone_number', DB::raw('count(*) as number_count'))
                ->groupBy('phone_number')
                ->havingRaw('count(*) > 1')
                ->get();

        echo "<br>";
        echo "Deleting Duplicates... <br> ";
        echo "<br>";

        foreach ($users as $userdelete) {
            // Get the row you don't want to delete.
            $dontDeleteThisRow = TableOne::where('phone_number', $userdelete->phone_number)->first();

            // Delete all rows except the one we fetched above.
            TableOne::where('phone_number', $userdelete->phone_number)->where('id', '!=', $dontDeleteThisRow->id)->delete();

            echo "Preserving One Number With id: " . $dontDeleteThisRow->id . ". Deleting all others. <br>";

        }

        echo "<br>";
        echo "Duplicates deleted! <br> ";
        echo "<br>";
    }

    public function updateBatchNumber()
    {
        $batchsize=300;

        DB::table('tableone')
            ->whereNull('batch_number')
            ->update(['batch_number' => 'id'/'batchsize']);

        // TableOne::where('batch_number', 0)
        //   ->update(['batch_number' => DB::raw( CEIL('id'/$batchsize) )]);
    }

     public function endqueue()
    {
        
        DB::table('tablethree')->truncate();

    }

     public function sendsms($phone_number, $message_text)
    {
        $client = new Client(config('services.twilio')['accountSid'], config('services.twilio')['authToken']);

        $sendnumber = '+1' . $phone_number;

        $client->messages->create(
            $sendnumber,
            array(
                'messagingServiceSid' => config('services.twilio')['messageService'],
                'body' => $message_text
            )
        );
        

    }

    public function getmaxbatch()
    {
        $max = TableThree::max('batch_number');

        $i = 1;
        while ($i <= $max)
        {
            $tosends = DB::table('tablethree')
                ->select('id', 'phone_number', 'message_text', 'batch_number')
                ->where('batch_number', $i)
                ->get();

            foreach ($tosends as $smsout) {
                
                $this->sendsms($smsout->phone_number, $smsout->message_text);

                echo "<br>";
                echo "sms sent to " . $smsout->phone_number;
                echo "<br>";

                $newfour = new TableFour;
                $newfour->batch_number = $smsout->batch_number;
                $newfour->phone_number = $smsout->phone_number;
                $newfour->message_text = $smsout->message_text;
                $newfour->save();

                echo "<br>";
                echo "written to tablefour";
                echo "<br>";

                $fourdeletethree = TableThree::find($smsout->id);
                $fourdeletethree->delete();

                echo "<br>";
                echo "deleted from tablethree";
                echo "<br>";

            }
        }
        $i++;
    }

   


    private function sendMessage($phoneNumber, $message, $imageUrl = null)
    {
        $twilioPhoneNumber = config('services.twilio')['phoneNumber'];
        $messageParams = array(
            'from' => $twilioPhoneNumber,
            'body' => $message
        );
        if ($imageUrl) {
            $messageParams['mediaUrl'] = $imageUrl;
        }

        $this->client->messages->create(
            $phoneNumber,
            $messageParams
        );
    }

    public function moveToTableTwo()
    {
        DB::table('tabletwo')->truncate();

        DB::statement('insert into tabletwo (phone_number, message_text, num_qty) SELECT phone_number, message_text, id FROM tableone');

        echo "Tabletwo created and populated.";
    }



    public function moveToTableThree()
    {
         DB::table('tablethree')->truncate();

         DB::statement('insert into tablethree (row_number, batch_number, phone_number, message_text) SELECT num_qty, CEIL(num_qty/300), phone_number, message_text FROM tabletwo');

         echo "Tablethree created and populated.";
    }

    public function combinedmove()
    {
        
        $this->moveToTableTwo();

        $this->moveToTableThree();

        echo "combined process complete.";

    }


}
