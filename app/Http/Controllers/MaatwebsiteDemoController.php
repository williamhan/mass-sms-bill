<?php

namespace App\Http\Controllers;

use Input;
use App\Item;
//use Request;
use Illuminate\Http\Request;
use App\TableOne;
use DB;
use Excel;
use League\Csv\Reader;
class MaatwebsiteDemoController extends Controller
{
	public function importExport()
	{
		return view('importExport');
	}
	public function downloadExcel($type)
	{
		$data = Item::get()->toArray();
		return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
	}
	public function importExcel(Request $request)
    {
        //if($request->hasFile('import_file')){


        	//$file = Request::file('file');


    	// $results = $reader->get();

            // Excel::load(Request::file('import_file')->getRealPath(), function ($reader) {
            //     foreach ($reader->toArray() as $key => $row) {
            //         $data['phone_number'] = $row['phone_number'];
            //         $data['message_text'] = $row['message_text'];

            //         if(!empty($data)) {
            //             DB::table('tableone')->insert($data);
            //         }
            //     }
            // });

			$csvfile = $request->file('import_file')->move(public_path("/uploads"), 'import_file.csv');

			$csv = Reader::createFromPath('uploads/import_file.csv')->setHeaderOffset(0);

			foreach ($csv as $record) 
			{
    			//Do not forget to validate your data before inserting it in your database
				TableOne::create([
                   'phone_number' => $record['phone_number'],
                   'message_text' => $record['message_text'],
               ]);
			}

  //   	if($request->hasFile('import_file')){
		//     $file = $request->file('import_file')->move(public_path("/uploads"), 'import_file.csv');
		//     $moved_file = 'uploads/import_file.csv';
		//     //$reference = $request->input('reference');
		//      \Excel::filter('chunk')->load($moved_file)->chunk(500, function($result){
		//         foreach ($result->toArray() as $subarray) {
		           
		//                 TableOne::create([
  //                  'phone_number' => $subarray['phone_number'],
  //                  'message_text' => $subarray['message_text'],
  //              ]);
		//            }
		        
		//        });
		//       return 'List Uploaded To Database';
		// }








   //  		$csvfile = Request::file('import_file');

   //          Excel::filter('chunk')->load($csvfile)->chunk(250, function($results)
			// {
			//         foreach($results->toArray() as $key => $row)
			//         {
			//             $data['phone_number'] = $row['phone_number'];
   //                  $data['message_text'] = $row['message_text'];

   //                  if(!empty($data)) {
   //                      DB::table('tableone')->insert($data);
   //                  }
			//         }
			// });
   //      //}

        return 'List Uploaded To Database';

   //      print_r($data);
    }
}