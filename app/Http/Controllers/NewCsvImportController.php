<?php

namespace App\Http\Controllers;

use Request;
use Response;
use DB;
use App\TableOne;
use App\TableTwo;
use App\TableThree;





class NewCsvImportController extends Controller
{
    private function _import_csv($path, $filename)
    {

        DB::table('tableone')->truncate();

        $csv = $path . $filename;

        //$fixedquery = sprintf("LOAD DATA INFILE '%s' INTO TABLE tableone FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;", addslashes($csv));


        $newquery = sprintf("LOAD DATA LOCAL INFILE '%s' INTO TABLE tableone CHARACTER SET utf8 FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES (phone_number, message_text)", addslashes($csv));
        return DB::connection()->getpdo()->exec($newquery);
    }

 private function _import_csv_unsubs($path, $filename)
    {

        DB::table('dnc_numbers')->truncate();

        $csv = $path . $filename;
        //     $query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE users FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 0 LINES (`firstname`, `lastname`, `username`, `gender`, `email`, `country`, `ethnicity`, `education`  )", addslashes($csv));
        // return DB::connection()->getpdo()->exec($query);

        $newquery = sprintf("LOAD DATA LOCAL INFILE '%s' INTO TABLE dnc_numbers CHARACTER SET utf8 FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' IGNORE 1 LINES (phone_number)", addslashes($csv));
        return DB::connection()->getpdo()->exec($newquery);
    }

public function uploadUnsubs()
    {


        if (Request::hasFile('do_not_contact')){

        $file = Request::file('do_not_contact');
        $name = time() . '-' . $file->getClientOriginalName();

        $path = 'public/uploads/'.date('Y/m/');

        $file->move($path, $name);
        $this->_import_csv_unsubs($path, $name);

        }

        echo 'File Uploaded to Database: <br><br>' . Response::json(["success"=>true]) . "<br><br>";
    }


 public function moveToTableTwo()
    {
        DB::table('tabletwo')->truncate();

        DB::statement('insert into tabletwo (phone_number, message_text, num_qty) SELECT phone_number, message_text, id FROM tableone');

        echo "<br>";
        echo "Tabletwo created and populated.";
        echo "<br>";
    }



    public function moveToTableThree()
    {
         DB::table('tablethree')->truncate();

         DB::statement('insert into tablethree (row_number, batch_number, phone_number, message_text) SELECT num_qty, CEIL(num_qty/300), phone_number, message_text FROM tabletwo');

         echo "<br>";
         echo "Tablethree created and populated.";
         echo "<br>";
    }

    public function cleanlist() 
    {
        $servername = "107.180.51.77";
        $username = "drd_1";
        $password = "Us3th3forc3!";
        $dbname = "twilionono";


        $conn = new mysqli($servername, $username, $password, $dbname);
    
        if ($conn->connect_error) 
        {
            die("Connection failed: " . $conn->connect_error);
        } 


        $sql = "SELECT * from tablethree WHERE batch_number = (SELECT MIN(batch_number) FROM tablethree)";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) 
        {
            while($row = $result->fetch_assoc()) 
            {
                $sqlfind = $conn->query("select * from dnc_numbers WHERE phone_number = '1" . $row["phone_number"] . "'");

                $sqldelete = "delete from tablethree WHERE id = " . $row["id"] . "";

                $sqlmove = "insert into tablethreepointfive (sent_date, batch_number, phone_number, message_text) SELECT NOW(), batch_number, phone_number, message_text from tablethree WHERE id = " . $row["id"] . "";

                while($newrow = $sqlfind->fetch_assoc()) 
                {
                    echo "Row Number " . $row["id"] . " Number " . $newrow["phone_number"] . " Found In DNC List. \n";

                    $deleteresult = $conn->query($sqldelete);

                    echo "Row Number " . $row["id"] . " Number " . $newrow["phone_number"] . " Removed from tablethree. \n";
                }

                // if ($sqldelete > 0)
                // {
                //  echo "Number " . $row["phone_number"] . " Found In DNC List. \n";
                // }
                // else
                // {
                //  echo "Number " . $row["phone_number"] . " Not In DNC List. Good To Send. \n";
                // }
            }
        } 
        else 
        {
            echo "0 results";
        }
    }

    public function combinedmove()
    {
        
        $this->moveToTableTwo();

        $this->moveToTableThree();

        echo "<br>";
        echo "combined process complete.";
        echo "<br>";

    }



public function uploadUsers(){


    if (Request::hasFile('import_file')){

        $file = Request::file('import_file');
        $name = time() . '-' . $file->getClientOriginalName();

        $path = 'public/uploads/'.date('Y/m/');

        $file->move($path, $name);
        $this->_import_csv($path, $name);

    }

    echo 'File Upload to Database: ' . Response::json(["success"=>true]) . "<br><br>";

    $users = DB::table('tableone')
                ->select('phone_number', DB::raw('count(*) as number_count'))
                ->groupBy('phone_number')
                ->havingRaw('count(*) > 1')
                ->get();

        foreach ($users as $user) {
            echo "Phone Number: " . $user->phone_number . " Number Of Instances: " . $user->number_count . "<br>";
            echo "<br>";
        }

        $userdels = DB::table('tableone')
                ->select('phone_number', DB::raw('count(*) as number_count'))
                ->groupBy('phone_number')
                ->havingRaw('count(*) > 1')
                ->get();

        echo "<br>";
        echo "Deleting Duplicates... <br> ";
        echo "<br>";

        foreach ($userdels as $userdelete) {
            // Get the row you don't want to delete.
            $dontDeleteThisRow = TableOne::where('phone_number', $userdelete->phone_number)->first();

            // Delete all rows except the one we fetched above.
            TableOne::where('phone_number', $userdelete->phone_number)->where('id', '!=', $dontDeleteThisRow->id)->delete();

            echo "Preserving One Number With id: " . $dontDeleteThisRow->id . ". Deleting all others. <br>";



        }

        echo "<br>";
        echo "Duplicates deleted! <br> ";
        echo "<br>";


        $this->combinedmove();
    

    return 'File Uploaded to Database, and Duplicates Removed. Ready for Sending.';
}
}
