<?php

namespace App\Http\Controllers;

use Request;
use App\TableOne;
use DB;

class NewCsvController extends Controller
{
    

	function csvToArray($filename = '', $delimiter = ',')
	{
    	if (!file_exists($filename) || !is_readable($filename))
        	return false;

    	$header = null;
    	$data = array();
    	if (($handle = fopen($filename, 'r')) !== false)
    	{
        	while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        	{
            	if (!$header)
                	$header = $row;
            	else
                	$data[] = array_combine($header, $row);
        	}
        	fclose($handle);
    	}
    	return $data;
	}

	public function dumpintodb()
	{
		
    	
//connect to Database
// mysql_select_db($database_csv, $csv);

// $servername = "107.180.51.77";
//     $username = "drd_1";
//     $password = "Us3th3forc3!";
//     $dbname = "twilionono";


// $mysqli = new mysqli($servername, $username, $password, $dbname);
//$mysqli = new mysqli('107.180.51.77','drd_1','Us3th3forc3!','twilionono');


/* check connection */
// if ($mysqli->connect_errno) {
//     printf("Connect failed: %s\n", $mysqli->connect_error);
//     exit();
// }
// else 
//             {
//                 //echo" connection successfully made <br>";
//             }

//if (isset($_POST['submit'])) {

    // $tmp = Request::file('import_file')->getFileName();
    // $name = $_FILES['csv_file']['name'];

    // //Can be any full path, just don't end with a /. That will be added in in the path variable
    // $uploads_dir = 'uploads';

    // $path = $uploads_dir.'/'.$name;
    // $pathtwo = addslashes($_FILES['csv_file']['name']);

    $name = Request::file('import_file')->getClientOriginalName();

	Request::file('import_file')->move(public_path().'/storage/directory', $name);

	$newpath = public_path().'/storage/directory'.$name;

    //if(move_uploaded_file($tmp, $path)){
        //echo "<br><center><p>". $name ."</p></center>";
        //echo "<br><center><p>" . $path ."</p></center>";

        //Import uploaded file to Database
        //If the query fails, try LOAD DATA LOCAL INFILE
        $import = "
        LOAD DATA INFILE '".$newpath."'
               INTO TABLE tableonetest  CHARACTER SET utf8 FIELDS TERMINATED BY ','
               OPTIONALLY ENCLOSED BY '\"' IGNORE 1 LINES (phone_number, message_text);
        ";

//         if ($mysqli->query("CREATE TABLE tableonetestagain LIKE tableonetest") === TRUE) {
//     printf("Table tableonetestagain successfully created.\n");
// }
// else 
//             {
//                 echo($mysqli->error);
//                 echo "<br>";
//             }

        DB::statement($import);

        return 'SMS Process began for: ' . $name .  '<br>';
        
        // if ($mysqli->query($import) === TRUE) 
        // {
        //     printf("SMS Process began for: " . $name .  " \n");
        // }
        // else 
        //     {
        //         echo($mysqli->error);
        //         echo "<br>";
        //     }

        //mysql_query($import) or die(mysql_error());
        //If you do not want to keep the csv, you can delete it after this point.
        //unlink($path);

    // }else{
    //     echo 'Failed to move uploaded files';
    // }

//}

//$mysqli->close();




    	// foreach ($allRows as $set)
     //    {
     //        Location::insert($set);
     //    }
	//}
}

function dump($data)
{
  echo "<pre>";
  //var_dump($data); 
  print_r($data); 
  echo "</pre>";
}
	public function postindex()
	{
		$file = Request::file('import_file');

		$runfile = $this->csvToArray($file);

		return dump($runfile);
	}



    public function add_valid_rows_to_array($reader, $headings)
    {
        $allRows = [];
        $allRowsInterval = 0;
        $rowCounter = 0;

        foreach ($reader as $index => $row)
        {
            $values = [];
            foreach ($headings as $key => $position)
            {
                if (!isset($row[$position]) || is_null($row[$position]))
                {
                    $row[$position] = '';
                }

                $values[0][$key] = $row[$position];
            }
            
            $allRows[$allRowsInterval][] = $values[0];
           
            $rowCounter ++;
            if ($rowCounter > 1000)
            {
                $allRowsInterval ++;
                $rowCounter = 0;
            }
        }

        return $allRows;
    }
public function insert_records_into_location_table($allRows)
    {
        foreach ($allRows as $set)
        {
            Location::insert($set);
        }
    }
}
