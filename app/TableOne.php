<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableOne extends Model
{
    public $table = 'tableone';

    public $fillable = [
        'phone_number', 'message_text', 'batch_number',
    ];
}
