<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptOutNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_out_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from_number', 15)->nullable();
            $table->string('to_number', 15)->nullable();
            $table->string('msid', 36)->nullable();
            $table->text('msid_name')->nullable();
            $table->text('body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opt_out_numbers');
    }
}
