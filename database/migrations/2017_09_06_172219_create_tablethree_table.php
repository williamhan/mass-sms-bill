<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablethreeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tablethree', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('row_number')->length(11)->unsigned()->nullable();
            $table->integer('batch_number')->length(11)->unsigned()->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->text('message_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tablethree');
    }
}
